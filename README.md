### Installation

```sh
 $ npm install
```
Copy .env.example to .env

### Start dev server

```sh
  $ npm start
```

### Data
 `src/constants/employees.js`
#### Structure
```sh
{
  id: 1,
  teamName: 'Company',
  name: 'Colly Bowton',
  designation: 'CTO',
  subEmployees: [
  {
    id: 2,
    teamName: 'DevOps',
    name: 'Millard Janout',
    designation: 'Lead',
    subEmployees: [
      ...
    ]
  },
  ...
}

```
