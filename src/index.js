import React from 'react';
import ReactDOM from 'react-dom';
import { Router, browserHistory } from 'react-router';
import { Provider } from 'react-redux';

// import routes from './routes';
// import store from './store';
import App from 'components/app';

ReactDOM.render(
  <App />
  , document.getElementById('container')
);
