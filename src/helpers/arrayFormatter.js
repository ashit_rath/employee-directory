const compute = (employee) => {
  employee.depthCount = employee.subEmployees.length;

  if (employee.depthCount === 0)
    return employee;

  for (let i = 0; i < employee.subEmployees.length; i++) {
    employee.subEmployees[i] = compute(employee.subEmployees[i]);
    employee.depthCount += employee.subEmployees[i].depthCount;
  }

  return employee
}

export {
  compute
}
