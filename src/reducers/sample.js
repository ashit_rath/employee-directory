import { handleActions } from 'redux-actions'

const reducer = {}
const initialState = {}

reducer['SAMPLE'] = (state, action) => {
  return state;
}

export default handleActions(reducer, initialState);
