import React, { Component } from 'react';
import PropTypes from 'prop-types'
import Card from 'components/Card';

class TreeNode extends Component {
  constructor(props) {
    super(props)

    this.state = {
      isOpen: false,
      shouldOpen: false,
      level: props.level,
      id: props.employee.id
    }
  }

  componentDidUpdate(prevState, prevProps) {
    if (this.context.currentLevel === this.state.level) {
      if (this.context.currentId === this.state.id && this.state.isOpen !== this.state.shouldOpen)
        this.setState({ isOpen: this.state.shouldOpen })
      else if (this.context.currentId !== this.state.id && this.state.isOpen)
        this.setState({ isOpen: false, shouldOpen: false })
    }
  }

  renderTreeNodes = (employees) => {
    return employees.map(employee => {

      return (
        <TreeNode
          isVisible={this.state.isOpen ? true : false}
          employee={employee}
          ref={i => this.treeNode = i}
          level={this.props.level + 1}
          key={employee.id}
        />
      )
    })
  }

  onToggle = () => {
    const { level, id, shouldOpen } = this.state

    this.context.setLevelAndId(level, id)
    this.setState({ shouldOpen: !shouldOpen })
  }

  render() {
    const { isVisible, employee } = this.props,
          classList = isVisible ? '' : 'hidden',
          downArrow = this.state.isOpen ? 'show-down-arrow' : ''

    return (
      <li className={`${classList} clearfix`}>
          <Card
            data={employee}
            onHeaderClick={this.onToggle}
            isOpen={this.state.isOpen}
          />
        <ul className={`level-cards clearfix ${downArrow}`} >
          {this.renderTreeNodes(employee.subEmployees)}
        </ul>
      </li>
    )
  }
}

TreeNode.contextTypes = {
  currentId: PropTypes.number.isRequired,
  currentLevel: PropTypes.number.isRequired,
  setLevelAndId: PropTypes.func.isRequired
}

TreeNode.propTypes = {
  level: PropTypes.number.isRequired,
  employee: PropTypes.object.isRequired,
  isVisible: PropTypes.bool.isRequired
}

export default TreeNode;
