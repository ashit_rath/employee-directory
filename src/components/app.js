import React, { Component } from 'react';
import PropTypes from 'prop-types';

import TreeNode from 'components/TreeNode';
import employees from 'constants/employees';
import { compute } from 'helpers/arrayFormatter'

class App extends Component {
  constructor() {
    super()

    this.state = {
      currentLevel: 1,
      currentId: employees.id,
      employees: compute(employees)
    }
  }

  getChildContext() {
    return {
      currentLevel: this.state.currentLevel,
      currentId: this.state.currentId,
      setLevelAndId: this.setLevelAndId
    }
  }

  setLevelAndId = (currentLevel, currentId) => {
    this.setState({ currentLevel, currentId })
  }

  render() {
    return (
      <div className="main">
        <ul className="clearfix">
          <TreeNode
            employee={employees}
            level={1}
            isVisible
          />
        </ul>
      </div>
    )
  }
}

App.childContextTypes = {
  currentId: PropTypes.number.isRequired,
  currentLevel: PropTypes.number.isRequired,
  setLevelAndId: PropTypes.func.isRequired
}

export default App;
