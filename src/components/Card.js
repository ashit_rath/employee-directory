import React from 'react';
import PropTypes from 'prop-types';
import FA from 'react-fontawesome';

const Card = (props) => {
  const { teamName, name, designation, depthCount, subEmployees } = props.data
  let headerClass = props.isOpen ? 'open' : ''

  headerClass += subEmployees.length ? '' : 'no-cursor'

  return (
    <div className="card-wrapper">
      <section
        className={`header ${headerClass}`}
        onClick={() => subEmployees.length ? props.onHeaderClick() : null}
      >
        <h6>{teamName}</h6>
      </section>
      <section className="personal-details">
        <p className="name">{name}</p>
        <p className="team-designation">{designation}</p>
      </section>
      <section className="team-details">
        <div className="immediate-employees"><FA name='sitemap' />{subEmployees.length}</div>
        <div className="total-employees"><FA name='user' />{depthCount}</div>
      </section>
    </div>
  )
}

Card.propTypes = {
  data: PropTypes.object.isRequired,
  onHeaderClick: PropTypes.func.isRequired,
  isOpen: PropTypes.bool.isRequired
}

export default Card;
