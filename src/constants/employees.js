const employees = {
  id: 1,
  teamName: 'Company',
  name: 'Colly Bowton',
  designation: 'CTO',
  subEmployees: [
    {
      id: 2,
      teamName: 'DevOps',
      name: 'Millard Janout',
      designation: 'Lead',
      subEmployees: [
        {
          id: 3,
          teamName: 'DevOps',
          name: 'Tuck Reeks',
          designation: 'Developer',
          subEmployees: [
            {
              id: 4,
              teamName: 'DevOps',
              name: 'Winni Stanfieldh',
              designation: 'Intern',
              subEmployees: []
            }
          ]
        }, {
          id: 5,
          teamName: 'DevOps',
          name: 'Ranice Greenough',
          designation: 'Developer',
          subEmployees: []
        }, {
          id: 6,
          teamName: 'DevOps',
          name: 'Algernon Fryatt',
          designation: 'Developer',
          subEmployees: []
        }, {
          id: 12,
          teamName: 'DevOps',
          name: 'Algernon Fryatt',
          designation: 'Developer',
          subEmployees: []
        }
      ]
    }, {
      id: 7,
      teamName: 'Platform',
      name: 'Fee Dinjes',
      designation: 'Lead',
      subEmployees: [
        {
          id: 5,
          teamName: 'DevOps',
          name: 'Ranice Greenough',
          designation: 'Developer',
          subEmployees: []
        }, {
          id: 6,
          teamName: 'DevOps',
          name: 'Algernon Fryatt',
          designation: 'Developer',
          subEmployees: []
        }
      ]
    }, {
      id: 8,
      teamName: 'Front End',
      name: 'Millard Janout',
      designation: 'Lead',
      subEmployees: [{
          id: 6,
          teamName: 'DevOps',
          name: 'Algernon Fryatt',
          designation: 'Developer',
          subEmployees: []
        }]
    },
    {
      id: 13,
      teamName: 'DevOps',
      name: 'Winni Stanfieldh',
      designation: 'Intern',
      subEmployees: []
    }
  ]
}

export default employees;
